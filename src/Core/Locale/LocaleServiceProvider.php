<?php

namespace Core\Locale;

use Silex\Provider\LocaleServiceProvider as BaseProvider;
use Pimple\Container;

class LocaleServiceProvider extends BaseProvider
{
    public function register(Container $app)
    {
        $app['locale.listener'] = function ($app) {
            return new LocaleListener($app, $app['locale'], $app['request_stack'], isset($app['request_context']) ? $app['request_context'] : null);
        };
    }
}