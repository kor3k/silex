<?php

namespace Core\Locale;

use Pimple\Container;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\Event\FinishRequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Initializes the locale based on the current request and session.
 */
class LocaleListener implements EventSubscriberInterface
{
    private $app;
    private $defaultLocale;
    private $requestStack;
    private $requestContext;

    public function __construct(Container $app, $defaultLocale = 'en', RequestStack $requestStack, RequestContext $requestContext = null)
    {
        $this->app = $app;
        $this->defaultLocale = $defaultLocale;
        $this->requestStack = $requestStack;
        $this->requestContext = $requestContext;
    }

    public function onKernelRequest(GetResponseEvent $event)
    {
        $request = $event->getRequest();
        $request->setDefaultLocale($this->defaultLocale);

        if( $request->query->has('_locale') ) //if request has _locale attribute
        {
            $this->app['locale']    =   $request->query->get('_locale'); //update app locale

            if( $this->hasSession() )
            {
                $this->app['session']->set( '_locale' , $this->app['locale'] ); //update session locale
            }
        }
        else //use session locale if available, fallback to default locale
        {
            if( $this->hasSession() )
            {
                $this->app['locale']    =   $this->app['session']->get( '_locale' , $this->defaultLocale );
            }
            else
            {
                $this->app['locale']    =   $this->defaultLocale;
            }
        }

        $request->setLocale( $this->app['locale'] ); //set locale to request
        $this->setRouterContext($request);

        $this->loadFormAndValidatorTranslations();
    }

    protected function loadFormAndValidatorTranslations()
    {
        $app        =   $this->app;
        $translator =   $this->app['translator'];

        //TranslationServiceProvider only loads resources for current $app['locale'] at the boot time
        //so we need to load resources for the locale set by this listener

        if (isset($app['validator'])) {
            $r = new \ReflectionClass('Symfony\Component\Validator\Validation');
            $file = dirname($r->getFilename()).'/Resources/translations/validators.'.$app['locale'].'.xlf';
            if (file_exists($file)) {
                $translator->addResource('xliff', $file, $app['locale'], 'validators');
            }
        }

        if (isset($app['form.factory'])) {
            $r = new \ReflectionClass('Symfony\Component\Form\Form');
            $file = dirname($r->getFilename()).'/Resources/translations/validators.'.$app['locale'].'.xlf';
            if (file_exists($file)) {
                $translator->addResource('xliff', $file, $app['locale'], 'validators');
            }
        }
    }

    public function onKernelFinishRequest(FinishRequestEvent $event)
    {
        if (null !== $parentRequest = $this->requestStack->getParentRequest()) {
            $this->setRouterContext($parentRequest);
        }
    }

    private function hasSession()
    {
        return isset( $this->app['session'] );
    }

    private function setRouterContext(Request $request)
    {
        if (null !== $this->requestContext) {
            $this->requestContext->setParameter('_locale', $request->getLocale());
        }
    }

    public static function getSubscribedEvents()
    {
        return array(
            // must be registered after the Router to have access to the _locale
            KernelEvents::REQUEST => array(array('onKernelRequest', 16)),
            KernelEvents::FINISH_REQUEST => array(array('onKernelFinishRequest', 0)),
        );
    }
}
